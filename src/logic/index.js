import fetch from "isomorphic-fetch";

export const getAllJobs = async () => {
  try {
    const response = await fetch("http://localhost:4444/jobs/alljobs");
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
    return await response.json();
  } catch {
    console.log("error in fetching data from server");
  }
};

export const addJob = async (body) => {
  try {
   await fetch("http://localhost:4444/jobs/addjob", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body,
    });
  } catch {
    console.error("error in sending job to server");
  }
};
export const delJob = async (body) => {
  try {
   await fetch("http://localhost:4444/jobs/deljob", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body,
    });
  } catch {
    console.error("error in deleting job");
  }
};
