# The Excuser React App

A front end that connects to a Vonage voice app that allows the user to call in and schedule a callback.  At present it allows the user to specify a date and time for a return call, and a phone number to call at.  

Currently the date-time picker only works properly in Chrome.


## Prepare


`yarn`


## Run

`yarn start`


## Requires

Node v. 15.x
