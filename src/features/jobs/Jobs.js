import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import DateTimePicker from "react-datetime-picker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faMobileAlt,
  faCalendarCheck,
  faTrash,
  faClipboardList,
} from "@fortawesome/free-solid-svg-icons";

import { getJobs, addOneJob, deleteJob, allJobsSelector } from "./jobsSlice";
import styles from "./Jobs.module.css";

export function Jobs() {
  const allJobs = useSelector(allJobsSelector);
  const dispatch = useDispatch();
  const [dateTime, setDateTime] = useState(new Date());
  const [phoneNumber, setPhoneNumber] = useState("");

  useEffect(() => {
    const timer = setInterval(() => {
      dispatch(getJobs());
    }, 2000);
    return () => clearInterval(timer);
  });

  const changePhoneNumber = (e) => {
    setPhoneNumber(e.target.value);
  };

  const delJob = (id) => {
    dispatch(deleteJob(JSON.stringify({ id })));
  };

  const makeNewJob = (e) => {
    e.preventDefault();
    dispatch(
      addOneJob(
        JSON.stringify({ job: { time: dateTime.toJSON(), to: phoneNumber } })
      )
    );
  };

  return (
    <div className={styles.jobs_main}>
      <h1>The Excuser</h1>
      <div className={styles.flexColumn}>
        <h2>
          <FontAwesomeIcon icon={faCalendarCheck} size={"5x"} />
          &nbsp;Schedule A Call
        </h2>

        <form onSubmit={makeNewJob}>
          <div className={styles.inputGroup}>
            <label>
              <FontAwesomeIcon icon={faClock} size={"3x"} />
              &nbsp; Call Time
            </label>
            <DateTimePicker onChange={setDateTime} value={dateTime} />
          </div>
          <div className={styles.inputGroup}>
            <label>
              <FontAwesomeIcon icon={faMobileAlt} size={"3x"} />
              &nbsp; Number
            </label>

            <input
              type="text"
              onChange={changePhoneNumber}
              id="phoneNumber"
              name="phoneNumber"
              value={phoneNumber}
            />
          </div>

          <button className={styles.submitButton}>Submit</button>
        </form>
      </div>
      <div className={styles.flexColumn}>
        <h2>
          <FontAwesomeIcon icon={faClipboardList} size={"5x"} />
          &nbsp;Scheduled Calls
        </h2>
        <table>
          <thead>
            <tr>
              <th>Number</th>
              <th>Time</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {allJobs && allJobs.length ? (
              allJobs.map((job) => (
                <tr>
                  <td>{job.to}</td>
                  <td>{job.time}</td>
                  <td>
                    <FontAwesomeIcon
                      onClick={() => delJob(job.uuid)}
                      key={job.uuid}
                      icon={faTrash}
                      size={"lg"}
                      className={styles.trash}
                    />
                  </td>
                </tr>
              ))
            ) : (
              <tr>
                <td></td>
                <td>
                  No jobs
                  <br />
                  scheduled
                </td>

                <td></td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}
