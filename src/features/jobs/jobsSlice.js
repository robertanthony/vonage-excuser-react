import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { parseJSON, format } from "date-fns";
import { getAllJobs, addJob, delJob} from "../../logic/index.js";

export const getJobs = createAsyncThunk("jobs/alljobs", async (_, thunkAPI) => {
  
  return await getAllJobs();
});
export const addOneJob = createAsyncThunk("jobs/alljobs", async (body, thunkAPI) => {
   await addJob(body);
});
export const deleteJob = createAsyncThunk("jobs/deletejob", async (body, thunkAPI) => {
   await delJob(body);
});

export const jobsSlice = createSlice({
  name: "jobs",
  initialState: {
    allJobs: [],
  },
  reducers: {},
  extraReducers: {
    // Add reducers for additional action types here, and handle loading state as needed
    [getJobs.fulfilled]: (state, action) => {
      const rawJobs = action.payload;
      if (!rawJobs || rawJobs.length === 0) {
        state.allJobs = [];

        return;
      };
      state.allJobs = rawJobs.map((job) => {
        const newTime = format(parseJSON(job.time), "dd/MM/yyyy -> HH:mm:ss");
        const newJob = { uuid: job.uuid, to: job.to, time: newTime };

        return newJob;
      });
      
    },
  },
});

export const allJobsSelector = (state) => state.jobs.allJobs;


export default jobsSlice.reducer;
